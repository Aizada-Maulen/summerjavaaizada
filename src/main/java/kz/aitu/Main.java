package kz.aitu;

import kz.aitu.entity.Student;

public class Main {
    public static void main(String[] args) {
        int a = 5;
        int b = 10;

        a += b;
        System.out.println("Hello, I am Aizada, welcome to my first programm");
        System.out.println("a is " + a);


        Student st1 = new Student();
        st1.setName("Treugol'nik");
        st1.setAge(22);
        st1.setGroupName("second It group");
        st1.setGpa(3.9);

        Student st2 = new Student();
        st2.setName("Aizada");
        st2.setAge(19);
        st2.setGroupName("first It group");
        st2.setGpa(3.5);

        Student st3 = new Student();
        st3.setName("Kamila");
        st3.setAge(18);
        st3.setGroupName("first It group");
        st3.setGpa(3.2);

        Student st4 = new Student();
        st4.setName("Aliya");
        st4.setAge(17);
        st4.setGroupName("second It group");
        st4.setGpa(3.0);

        Student st5 = new Student("NewName", 17, 2.7, "third It group");


        System.out.println("Output student");
        System.out.println("name: " + st1.getName());
        System.out.println("age: " + st1.getAge());
        System.out.println("group: " + st1.getGroupName());
        System.out.println("gpa: " + st1.getGpa());


        System.out.println(st1.toString());
        System.out.println(st2.toString());
        System.out.println(st3.toString());
        System.out.println(st4.toString());
        System.out.println(st5.toString());
    }

}
