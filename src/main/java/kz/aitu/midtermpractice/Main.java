package kz.aitu.midtermpractice;

public class Main {
    public static void main(String[] args) {

        Aquarium aq1 = new Aquarium(17.3, 310.1);


        Fish fish1 = new Fish(5, 2.8, "Billfish");
        Fish pr1 = new Fish(5, 1);
        Fish fish2 = new Fish(1.3, 0.4, "Mini");
        Fish pr2 = new Fish(1.3, 10);
        Fish fish3 = new Fish(35, 5, "Fire goby");
        Fish pr3 = new Fish(35, 10);

        Reptile reptile1 = new Reptile(100, 2.1, "Aakshde");
        Reptile prRept = new Reptile(100, 1);


        Accessories accessory1 = new Accessories(63.5, 0.7, "Lamp");
        Reptile prAcces = new Reptile(63.5, 1);

        System.out.println(aq1.getPrice() + aq1.getWeight());
        System.out.println("Aquarium Price = " + aq1.getTotalPrice());
        System.out.println("Fish1's price is " + fish1.getPrice());
        System.out.println("Reptile1' weight: " + reptile1.getWeight());
        System.out.println("total price with reptile " + prRept.getTotalPrice());
        System.out.println( "Prices for fish1, fish 2, fish3: " + pr1.getTotalPrice() + "   " + pr2.getTotalPrice() + "  " +  pr3.getTotalPrice());
        System.out.println(accessory1.nameOfAccessor);

    }
}
