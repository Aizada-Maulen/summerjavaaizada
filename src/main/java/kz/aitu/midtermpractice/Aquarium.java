package kz.aitu.midtermpractice;

public class Aquarium {
    private double weight;
    private double price;
    private double totalPrice = 0;
    public int quantity;


public double getPrice() {
    return price;
}

    public void setPrice(double price) {
        this.price = price;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }


    public double getTotalPrice() {
        return totalPrice;
    }



    public Aquarium(double weight, double price) {
        this.weight = weight;
        this.price = price;
        totalPrice = totalPrice + this.price;
    }
    public Aquarium( double price, int quantity) {
        this.price = price;
        this.quantity = quantity;
        totalPrice = totalPrice + (this.price * this.quantity);
    }
}

