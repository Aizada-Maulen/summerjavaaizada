package kz.aitu.midtermpractice;

public class Accessories extends Aquarium{
    int quantity;
    String nameOfAccessor;

    public Accessories(double price, double weight, String nameOfAccessor) {
        super(price, weight);

    }
    public Accessories(double price, int quantity) {
        super(price, quantity);
    }
}
